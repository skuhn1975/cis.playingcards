#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Rank{TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE};

enum Suit
{
	DIAMONDS,
	CLUBS,
	SPADES,
	HEARTS
};

struct Card
{
	Rank rank;
	Suit suit;
};

string enumSuit(int suit)
{
	switch(suit)
	{
	case 0:
		return "DIAMONDS";
	case 1:
		return "CLUBS"; 
	case 2:
		return "SPADES";
	case 3:
		return "HEARTS";
	default:
		return "Error";
	}
}

string enumRank(int rank)
{
	switch (rank)
	{
	case 2:
		return "TWO";
	case 3:
		return "THREE";
	case 4:
		return "FOUR";
	case 5:
		return "FIVE";
	case 6:
		return "SIX";
	case 7:
		return "SEVEN";
	case 8:
		return "EIGHT";
	case 9:
		return "NINE";
	case 10:
		return "TEN";
	case 11:
		return "JACK";
	case 12:
		return "QUEEN";
	case 13:
		return "KING";
	case 14:
		return "ACE";
	default:
		return "ERROR";
	}
}

void PrintCard(Card card)
{
	
	string charsuit = enumSuit(card.suit);
	string charrank = enumRank(card.rank);


	cout << "\nThe " << charrank << " of " << charsuit;
}

Card HighCard(Card card1, Card card2)
{
	
	return ((card1.rank > card2.rank) ? (card1) : (card2));
}

int main()
{
 
	//FOR TESTING THE MAPPING OF THE RANK and SUIT to NAMES
	/*
	Card card;
	card.rank = THREE;
	card.suit = SPADES;
	PrintCard(card);
	*/

	_getch();
	return 0;
}